<?php 

include 'koneksi.php';
$id_makanan = $_GET['id_makanan'];
$data = mysqli_query($koneksi, "SELECT *FROM daftar_menu WHERE id_makanan='$id_makanan'");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Edit Menu</h2>
  <form action="proses_updatemenu.php" method="POST">
  <?php foreach ($data as $value): ?>
    <div class="form-group">
      <label for="email">ID Makanan:</label>
      <input type="text" class="form-control" id="email" value="<?php echo $value['id_makanan'] ?>" name="id_makanan" readonly>
    </div>
    <div class="form-group">
      <label for="pwd">Nama Makanan:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['nama_makanan'] ?>" name="nama_makanan">
    </div>
    <div class="form-group">
      <label for="pwd">Jenis Makanan:</label>
      <p><input type="radio" name="jenis_makanan" value="Dessert"/>Dessert</p>
      <p><input type="radio" name="jenis_makanan" value="Main Course"/>Main Course</p>
      <p><input type="radio" name="jenis_makanan" value="Appetizer"/>Appetizer</p>
    </div>
    <div class="form-group">
      <label for="pwd">Harga:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['harga'] ?>" name="harga">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
<?php endforeach ?>

    <button type="submit" class="btn btn-primary">Update</button>
  </form>
</div>

</body>
</html>