<?php 

include 'koneksi.php';
$id_pembelian = $_GET['id_pembelian'];
$data = mysqli_query($koneksi, "SELECT *FROM data_pembelian WHERE id_pembelian='$id_pembelian'");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Edit Data Pembelian</h2>
  <form action="proses_updatepembelian.php" method="POST">
  <?php foreach ($data as $value): ?>
    <div class="form-group">
      <label for="email">ID Pembelian:</label>
      <input type="text" class="form-control" id="email" value="<?php echo $value['id_pembelian'] ?>" name="id_pembelian" readonly>
    </div>
    <div class="form-group">
      <label for="email">ID Makanan:</label>
      <input type="text" class="form-control" id="email" value="<?php echo $value['id_makanan'] ?>" name="id_makanan" readonly>
    </div>
    <div class="form-group">
      <label for="pwd">Tanggal:</label>
      <input type="date" class="form-control" id="pwd" value="<?php echo $value['tanggal'] ?>" name="tanggal">
    </div>
    <div class="form-group">
      <label for="pwd">Jumlah Pembelian:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['jumlah_pembelian'] ?>" name="jumlah_pembelian">
    </div>
    <div class="form-group">
      <label for="pwd">Total Pembelian:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['total_pembelian'] ?>" name="total_pembelian">
    </div>
    <div class="form-group">
      <label for="pwd">ID Karyawan yang Melayani:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['id_karyawan'] ?>" name="id_karyawan">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
<?php endforeach ?>

    <button type="submit" class="btn btn-primary">Update</button>
  </form>
</div>

</body>
</html>
