<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="widt h=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Tambah Data Pembelian</h2>
  <form action="input_pembelian.php" method="POST">
    <div class="form-group">
      <label for="email">ID Pembelian:</label>
      <input type="text" class="form-control" placeholder="Masukkan ID Pembelian" name="id_pembelian">
    </div>
    <div class="form-group">
      <label for="email">ID Makanan:</label>
      <input type="text" class="form-control" placeholder="Masukkan ID Makanan" name="id_makanan">
    </div>
    <div class="form-group">
      <label for="email">Tanggal:</label>
      <input type="Date" class="form-control" placeholder="Masukkan Tanggal" name="tanggal">
    </div>
    <div class="form-group">
      <label for="pwd">Jumlah Pembelian:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Jumlah Pembelian" name="jumlah_pembelian">
    </div>
    <div class="form-group">
      <label for="pwd">Total Harga Pembelian:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Total Pembelian" name="total_pembelian">
    </div>
    <div class="form-group">
      <label for="pwd">ID Karyawan yang Melayani:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan ID Karyawan" name="id_karyawan">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

</body>
</html>
